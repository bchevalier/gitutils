# Git Utilities

**Git Utilities** is a collection of extended Git commands that, among other things, facilitates working with multiple, inter-dependent Git repositories.

When working with multiple, inter-dependent Git repositories, feature requests and bug fixes often result in changes across multiple repositories.  This creates branch dependencies between repositories which can be difficult to manage as most Git developers are required to frequently switch between branches.  Git Utilities include many utilities that focus on easing this burden by quickly performing typical Git operations on multiple repositories with a single command.  Git Utilities also includes other utilities that are useful even when working with a single Git repository.

---

## Notes on Use
By default, Git Utilities commands assume that there exists a common root directory containing all repos, and that the command is being executed from within the root directory or any directory below the root.  To enable Git Utilities commands to be executed from outside of the root directory tree, the GITUTILS_REPO_ROOT environment variable must be set.

---

## Installation

#### Recommended Installation
The simplest approach is to run the following one-liner and then close and re-open your terminal:  
```
$ curl https://bchevalier.gitlab.io/gitutils/update.py -o - | python
```

Note that you may need to invoke the command with superuser privileges:  
```
$ curl https://bchevalier.gitlab.io/gitutils/update.py -o - | sudo python
``` 

#### Advanced Installation
For more advanced installation options, such as installing to custom locations or prefixes, download and extract the latest source tarball from [here](https://bchevalier.gitlab.io/gitutils/release/) and then run a command similar to the following  
```
$ python setup.py install --prefix=/opt/gitutils
```

Note that when installing to a non-standard location, you will need to ensure that the installed scripts are located on the system PATH.

---

See a list of available commands at [https://bchevalier.gitlab.io/gitutils/commands](https://bchevalier.gitlab.io/gitutils/commands)
