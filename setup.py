#!/usr/bin/env python
import os
from distutils.core import setup
import gitutils

setup(
    name='gitutils',
    version=gitutils.__version__,
    description='Git utilities for working with multiple repos',
    author='Bob Chevalier',
    author_email='bob@chevalier.us',
    url='https://www.python.org/sigs/distutils-sig/',
    packages=[ 'gitutils' ],
    scripts=[ 'scripts/' + x for x in os.listdir( os.path.join( os.path.dirname( __file__ ), 'scripts' ) ) ],
    package_data={ 'gitutils': [ 'gitutils-completion.bash' ] },
)
#    data_files=[('etc/bash_completion.d', ['gitutils-completion.bash'])],
