import os
import unittest
from test.repo_test_case import RepoTestCase
from gitutils.common import run_cmd, run_git_cmd, is_valid_branch

class TestIsValidBranch( RepoTestCase ):
    def setUp( self ):
        RepoTestCase.setUp( self )

        self.make_commit( self.REPO_DIR, 'A' )
        run_git_cmd( [ 'checkout', '-b', 'branch' ], self.REPO_DIR )

    def test_master( self ):
        self.assertTrue( is_valid_branch( self.REPO_DIR, 'master' ) )

    def test_branch( self ):
        self.assertTrue( is_valid_branch( self.REPO_DIR, 'branch' ) )

    def test_invalid_branch( self ):
        self.assertFalse( is_valid_branch( self.REPO_DIR, 'invalid_branch' ) )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestIsValidBranch )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
