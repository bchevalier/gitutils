import os
import sys
import unittest
from datetime import timedelta
from test.repo_test_case import RepoTestCase
from gitutils.common import run_cmd, run_git_cmd, git_tag

class TestGitTag( RepoTestCase ):
    def setUp( self ):
        RepoTestCase.setUp( self )

    def test_no_tags( self ):
        tags = git_tag( self.REPO_DIR )
        # an empty list evaluates to False
        self.assertFalse( tags )

    def test_single_tag( self ):
        self.make_commit( self.REPO_DIR, 'A' )
        first_tag = self.make_tag( self.REPO_DIR, self.get_tag_string( timedelta( 0 ), 1 ) )
        tags = git_tag( self.REPO_DIR )
        self.assert_lists_equal( tags, [ first_tag ] )

    def test_multiple_tags( self ):
        self.make_commit( self.REPO_DIR, 'A' )
        first_tag = self.make_tag( self.REPO_DIR, self.get_tag_string( timedelta( days=-1 ), 1 ) )
        self.make_commit( self.REPO_DIR, 'B' )
        second_tag = self.make_tag( self.REPO_DIR, self.get_tag_string( timedelta( 0 ), 1 ) )
        tags = git_tag( self.REPO_DIR )
        self.assert_lists_equal( tags, [ first_tag, second_tag ] )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestGitTag )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
