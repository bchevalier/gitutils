import os
import unittest
from test.repo_test_case import RepoTestCase
from gitutils.common import run_cmd, run_git_cmd, find_workspace_dir

class TestFindWorkspaceDir( RepoTestCase ):
    def setUp( self ):
        RepoTestCase.setUp( self )

    def test_workspace_dir( self ):
        cwd = os.getcwd()
        os.chdir( self.WORKSPACE_DIR )
        workspace_dir = find_workspace_dir()
        self.assertEqual( workspace_dir, self.WORKSPACE_DIR )
        os.chdir( cwd )

    def test_repo_dir( self ):
        cwd = os.getcwd()
        os.chdir( self.REPO_DIR )
        workspace_dir = find_workspace_dir()
        self.assertEqual( workspace_dir, self.WORKSPACE_DIR )
        os.chdir( cwd )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestFindWorkspaceDir )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
