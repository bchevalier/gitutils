import os
import unittest
from test.repo_test_case import RepoTestCase
from gitutils.common import run_cmd, run_git_cmd, git_checkout, current_branch

class TestGitCheckout( RepoTestCase ):
    def setUp( self ):
        RepoTestCase.setUp( self )

        self.make_commit( self.REPO_DIR, 'A' )
        run_git_cmd( [ 'checkout', '-b', 'branch' ], self.REPO_DIR )

    def test_valid_branch( self ):
        git_checkout( self.REPO_DIR, 'master', False )
        (is_detached, branch) = current_branch( self.REPO_DIR )
        self.assertEqual( branch, 'master' )

    def test_invalid_branch( self ):
        (returncode, output) = git_checkout( self.REPO_DIR, 'invalid_branch', False )
        self.assertEqual( returncode, 1 )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestGitCheckout )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
