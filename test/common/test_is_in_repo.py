import unittest
from test.repo_test_case import RepoTestCase
from gitutils.common import run_git_cmd, is_in_repo

class TestIsInRepo( RepoTestCase ):
    def setUp( self ):
        RepoTestCase.setUp( self )

    def test_parent_dir( self ):
        (in_repo, repo_base_dir) = is_in_repo( self.PARENT_DIR )
        self.assertFalse( in_repo )

    def test_workspace_dir( self ):
        (in_repo, repo_base_dir) = is_in_repo( self.WORKSPACE_DIR )
        self.assertFalse( in_repo )

    def test_repo_dir( self ):
        (in_repo, repo_base_dir) = is_in_repo( self.REPO_DIR )
        self.assertTrue( in_repo )
        self.assertEqual( repo_base_dir, self.REPO_DIR )

    def test_repo_sub_dir( self ):
        (in_repo, repo_base_dir) = is_in_repo( self.REPO_SUB_DIR )
        self.assertTrue( in_repo )
        self.assertEqual( repo_base_dir, self.REPO_DIR )

    def test_non_workpace_sub_dir( self ):
        (in_repo, repo_base_dir) = is_in_repo( self.NON_WORKSPACE_SUB_DIR )
        self.assertFalse( in_repo )

if __name__ == '__main__':
#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase( TestIsInRepo )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
