import os
import sys
import unittest
from datetime import timedelta
from test.repo_test_case import RepoTestCase
from gitutils.common import run_cmd, run_git_cmd, current_branch, git_checkout

class TestCurrentBranch( RepoTestCase ):
    def setUp( self ):
        RepoTestCase.setUp( self )

        self.make_commit( self.REPO_DIR, 'A' )
        self.FIRST_TAG = self.make_tag( self.REPO_DIR, self.get_tag_string( timedelta( 1 ), 1 ) )
        self.SECOND_TAG = self.make_tag( self.REPO_DIR, self.get_tag_string( timedelta( 0 ), 1 ) )

    def test_master( self ):
        (is_detached, branch_name) = current_branch( self.REPO_DIR )
        self.assertFalse( is_detached )
        self.assertEqual( branch_name, 'master' )

    def test_first_tag( self ):
        git_checkout( self.REPO_DIR, self.FIRST_TAG, False )
        (is_detached, branch_name) = current_branch( self.REPO_DIR )
        self.assertTrue( is_detached )
        self.assertEqual( branch_name, self.FIRST_TAG )

    def test_second_tag( self ):
        git_checkout( self.REPO_DIR, self.SECOND_TAG, False )
        (is_detached, branch_name) = current_branch( self.REPO_DIR )
        self.assertTrue( is_detached )
        self.assertEqual( branch_name, self.SECOND_TAG )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestCurrentBranch )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
