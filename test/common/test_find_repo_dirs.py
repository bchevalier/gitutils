import os
import unittest
from test.repo_test_case import RepoTestCase
from gitutils.common import run_git_cmd, find_repo_dirs

class TestFindRepoDirs( RepoTestCase ):
    def setUp( self ):
        RepoTestCase.setUp( self )

        # create a second repo in the workspace
        self.REPO2_DIR = os.path.join( self.WORKSPACE_DIR, 'repo2' )
        self.create_dir( self.REPO2_DIR )

        # initialize second git repo
        run_git_cmd( [ 'init' ], self.REPO2_DIR )

        self.REPO_DIRS = [ self.REPO_DIR, self.REPO2_DIR ]

    def test_parent_dir( self ):
        repo_dirs = find_repo_dirs( self.PARENT_DIR )
        self.assert_lists_equal( repo_dirs, [] )

    def test_workspace_dir( self ):
        repo_dirs = find_repo_dirs( self.WORKSPACE_DIR )
        self.assert_lists_equal( repo_dirs, self.REPO_DIRS )

    def test_repo_dir( self ):
        repo_dirs = find_repo_dirs( self.REPO_DIR )
        self.assert_lists_equal( repo_dirs, [ self.REPO_DIR ] )

    def test_repo2_dir( self ):
        repo_dirs = find_repo_dirs( self.REPO2_DIR )
        self.assert_lists_equal( repo_dirs, [ self.REPO2_DIR ] )

    def test_repo_sub_dir( self ):
        repo_dirs = find_repo_dirs( self.REPO_SUB_DIR )
        self.assert_lists_equal( repo_dirs, [ self.REPO_DIR ] )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestFindRepoDirs )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
