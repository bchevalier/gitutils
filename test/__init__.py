import os

tests_dir = os.path.realpath( os.path.dirname( __file__ ) )
pkg_dir = os.path.dirname( tests_dir )
scripts_dir = os.path.join( pkg_dir, 'scripts' )

# add top-level package directory to the python path
os.environ[ 'PYTHONPATH' ] = pkg_dir

# add scripts directory to the path
os.environ[ 'PATH' ] = scripts_dir + os.pathsep + os.environ[ 'PATH' ]
