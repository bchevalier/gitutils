import os
import unittest
from datetime import timedelta
from test.workspace_test_case import WorkspaceTestCase
from gitutils.common import run_git_cmd, current_branch

class TestGitLatestTags( WorkspaceTestCase ):
    def setUp( self ):
        WorkspaceTestCase.setUp( self )

        two_days_ago_str = self.get_tag_string( timedelta( days=-2 ), 1 )
        yesterday_str = self.get_tag_string( timedelta( days=-1 ), 1 )
        self.YESTERDAY_PATTERN = yesterday_str.rstrip( '1' )
        self.YESTERDAY_PATTERN = self.YESTERDAY_PATTERN.rstrip( '-' )
        tomorrow_str = self.get_tag_string( timedelta( days=1 ), 1 )

        # create a single valid tag in repo 1
        self.REPO1 = self.make_tag( self.REPO1_DIR, two_days_ago_str )

        # create three valid tags( one in the future ) in repo 2
        self.make_tag( self.REPO2_DIR, two_days_ago_str )
        self.make_commit( self.REPO2_DIR, 'B' )
        self.REPO2_YESTERDAY = self.make_tag( self.REPO2_DIR, yesterday_str )
        self.make_commit( self.REPO2_DIR, 'C' )
        self.REPO2_LATEST = self.make_tag( self.REPO2_DIR, tomorrow_str )

        # create an invalid tag in repo 3
        self.REPO3 = self.make_tag( self.REPO3_DIR, '2.3.4' )

    def test_from_workspace_without_filter( self ):
        (returncode, output) = run_git_cmd( [ 'latest-tags' ], self.WORKSPACE_DIR )
        self.assertTrue( self.REPO1 in current_branch( self.REPO1_DIR )[ 1 ] )
        self.assertTrue( self.REPO2_LATEST in current_branch( self.REPO2_DIR )[ 1 ] )
        self.assertTrue( self.REPO3 in current_branch( self.REPO3_DIR )[ 1 ] )

    def test_from_workspace_with_filter( self ):
        # make sure all repos start out in master
        (returncode, output) = run_git_cmd( [ 'checkout-all', 'master' ], self.WORKSPACE_DIR )

        (returncode, output) = run_git_cmd( [ 'latest-tags', '-m', self.YESTERDAY_PATTERN ], self.WORKSPACE_DIR )
        self.assertEqual( current_branch( self.REPO1_DIR )[ 1 ], 'master' )
        self.assertTrue( self.REPO2_YESTERDAY in current_branch( self.REPO2_DIR )[ 1 ] )
        self.assertEqual( current_branch( self.REPO3_DIR )[ 1 ], 'master' )

    def test_from_repo_without_filter( self ):
        (returncode, output) = run_git_cmd( [ 'latest-tags' ], self.REPO1_DIR )
        self.assertTrue( self.REPO1 in current_branch( self.REPO1_DIR )[ 1 ] )
        self.assertTrue( self.REPO2_LATEST in current_branch( self.REPO2_DIR )[ 1 ] )
        self.assertTrue( self.REPO3 in current_branch( self.REPO3_DIR )[ 1 ] )

    def test_from_repo_with_filter( self ):
        (returncode, output) = run_git_cmd( [ 'latest-tags', '-m', self.YESTERDAY_PATTERN ], self.REPO1_DIR )
        self.assertTrue( current_branch( self.REPO1_DIR )[ 1 ], 'master' )
        self.assertTrue( self.REPO2_YESTERDAY in current_branch( self.REPO2_DIR )[ 1 ] )
        self.assertEqual( current_branch( self.REPO3_DIR )[ 1 ], 'master' )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestGitLatestTags )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
