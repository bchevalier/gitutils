import unittest
import os
import shutil
import sys
import tempfile
from datetime import date
from gitutils.common import run_cmd, run_git_cmd

class BaseTestCase( unittest.TestCase ):
    def setUp( self ):
        # on macs, /tmp is a symbolic link to /private/tmp so we need to get realpath
        self.PARENT_DIR = os.path.realpath( os.path.join( tempfile.gettempdir(), 'gitutils' ) )

        # create workspace dir structure
        self.WORKSPACE_DIR = os.path.join( self.PARENT_DIR, 'workspace' )
        self.create_dir( self.WORKSPACE_DIR )

        # create non-workspace dir structure
        self.NON_WORKSPACE_DIR = os.path.join( self.PARENT_DIR, 'non_workspace' )
        self.NON_WORKSPACE_SUB_DIR = os.path.join( self.NON_WORKSPACE_DIR, 'sub' )
        self.create_dir( self.NON_WORKSPACE_SUB_DIR )

    def tearDown( self ):
        shutil.rmtree( self.PARENT_DIR )

    def create_dir( self, path ):
        try:
            os.makedirs( path )
        except OSError:
            if not os.path.isdir( path ):
                raise

    def make_commit( self, repo, commit_num ):
        filename = 'foo' + str( commit_num )
        commit_msg = 'Commit ' + str( commit_num )

        run_cmd( [ 'touch', filename ], repo )
        run_git_cmd( [ 'add', filename ], repo )
        run_git_cmd( [ 'commit', '-m', commit_msg ], repo )

    def get_tag_string( self, time_delta, revision ):
        return (date.today() + time_delta).strftime( '%Y.%m.%d' ) + '-' + str( revision )

    def make_tag( self, repo, tag_name ):
        run_git_cmd( [ 'tag', tag_name ], repo )
        return tag_name

    def make_and_checkout_branch( self, repo, parent_branch, branch_name ):
        run_git_cmd( [ 'checkout', parent_branch ], repo )
        run_git_cmd( [ 'checkout', '-b', branch_name ], repo )

    def assert_lists_equal( self, left, right ):
        if ( sys.version_info > (3, 0 ) ):
            return self.assertCountEqual( left, right )
        else:
            return self.assertItemsEqual( left, right )

