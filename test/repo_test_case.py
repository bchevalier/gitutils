import os
from test.base_test_case import BaseTestCase
from gitutils.common import run_git_cmd

class RepoTestCase( BaseTestCase ):
    def setUp( self ):
        BaseTestCase.setUp( self )

        # create repo dir structure
        self.REPO_DIR = os.path.join( self.WORKSPACE_DIR, 'repo' )
        self.REPO_SUB_DIR = os.path.join( self.REPO_DIR, 'sub' )
        self.create_dir( self.REPO_SUB_DIR )

        # initialize git repo
        run_git_cmd( [ 'init' ], self.REPO_DIR )
