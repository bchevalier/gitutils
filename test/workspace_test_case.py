import os
from test.base_test_case import BaseTestCase
from gitutils.common import run_git_cmd

class WorkspaceTestCase( BaseTestCase ):
    def setUp( self ):
        BaseTestCase.setUp( self )

        # create first repo
        self.REPO1_DIR = os.path.join( self.WORKSPACE_DIR, 'repo1' )
        self.create_dir( self.REPO1_DIR )
        (returncode, output) = run_git_cmd( [ 'init' ], self.REPO1_DIR )

        # create second repo
        self.REPO2_DIR = os.path.join( self.WORKSPACE_DIR, 'repo2' )
        self.create_dir( self.REPO2_DIR )
        (returncode, output) = run_git_cmd( [ 'init' ], self.REPO2_DIR )

        # create third repo
        self.REPO3_DIR = os.path.join( self.WORKSPACE_DIR, 'repo3' )
        self.create_dir( self.REPO3_DIR )
        (returncode, output) = run_git_cmd( [ 'init' ], self.REPO3_DIR )

        # create fourth repo
        self.REPO4_DIR = os.path.join( self.WORKSPACE_DIR, 'repo4' )
        self.create_dir( self.REPO4_DIR )
        (returncode, output) = run_git_cmd( [ 'init' ], self.REPO4_DIR )

        # create master branch, with initial commit, in each repo
        self.make_commit( self.REPO1_DIR, 'A' )
        self.make_commit( self.REPO2_DIR, 'A' )
        self.make_commit( self.REPO3_DIR, 'A' )
        self.make_commit( self.REPO4_DIR, 'A' )
