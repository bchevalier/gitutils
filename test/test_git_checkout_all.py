import os
import unittest
from test.workspace_test_case import WorkspaceTestCase
from gitutils.common import run_git_cmd, current_branch

class TestGitCheckoutAll( WorkspaceTestCase ):
    def setUp( self ):
        WorkspaceTestCase.setUp( self )

        # create a branch, based on master, in first and third repos
        self.make_and_checkout_branch( self.REPO1_DIR, 'master', 'branch_1' )
        self.make_and_checkout_branch( self.REPO3_DIR, 'master', 'branch_1' )

        # create a different branch, based on master, in second repo
        self.make_and_checkout_branch( self.REPO2_DIR, 'master', 'branch_2' )

    def test_master_from_workspace( self ):
        (returncode, output) = run_git_cmd( [ 'checkout-all', 'master' ], self.WORKSPACE_DIR )
        self.assertEqual( current_branch( self.REPO1_DIR )[ 1 ], 'master' )
        self.assertEqual( current_branch( self.REPO2_DIR )[ 1 ], 'master' )
        self.assertEqual( current_branch( self.REPO3_DIR )[ 1 ], 'master' )

    def test_master_from_repo( self ):
        (returncode, output) = run_git_cmd( [ 'checkout-all', 'master' ], self.REPO1_DIR )
        self.assertEqual( current_branch( self.REPO1_DIR )[ 1 ], 'master' )
        self.assertEqual( current_branch( self.REPO2_DIR )[ 1 ], 'master' )
        self.assertEqual( current_branch( self.REPO3_DIR )[ 1 ], 'master' )

    def test_branches_from_repo( self ):
        (returncode, output) = run_git_cmd( [ 'checkout-all', 'master' ], self.REPO1_DIR )
        self.assertEqual( current_branch( self.REPO1_DIR )[ 1 ], 'master' )
        self.assertEqual( current_branch( self.REPO2_DIR )[ 1 ], 'master' )
        self.assertEqual( current_branch( self.REPO3_DIR )[ 1 ], 'master' )

        (returncode, output) = run_git_cmd( [ 'checkout-all', 'branch_1' ], self.REPO1_DIR )
        self.assertEqual( current_branch( self.REPO1_DIR )[ 1 ], 'branch_1' )
        self.assertEqual( current_branch( self.REPO2_DIR )[ 1 ], 'master' )
        self.assertEqual( current_branch( self.REPO3_DIR )[ 1 ], 'branch_1' )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestGitCheckoutAll )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
