import os
import re
import sys
import unittest
from test.repo_test_case import RepoTestCase
from gitutils.common import run_cmd, run_git_cmd, git_tag

class TestGitForkPoint( RepoTestCase ):
    def setUp( self ):
        RepoTestCase.setUp( self )
        self.ANSI_REGEX = re.compile(r'\x1b[^m]*m')

        self.make_commit( self.REPO_DIR, 'A' )
        self.make_commit( self.REPO_DIR, 'B' )

        self.make_and_checkout_branch( self.REPO_DIR, 'master', 'branch' )
        (returncode, output) = run_git_cmd( [ 'rev-parse', 'HEAD' ], self.REPO_DIR )
        self.MERGE_HASH = output.strip()
        self.make_commit( self.REPO_DIR, 'C' )

        run_git_cmd( [ 'checkout', 'master' ], self.REPO_DIR )
        self.make_commit( self.REPO_DIR, 'D' )

        run_git_cmd( [ 'checkout', 'branch' ], self.REPO_DIR )
        run_git_cmd( [ 'merge', '--no-ff', 'master'], self.REPO_DIR )
        self.make_commit( self.REPO_DIR, 'E' )

        run_git_cmd( [ 'checkout', 'master' ], self.REPO_DIR )
        self.make_commit( self.REPO_DIR, 'F' )
        (returncode, output) = run_git_cmd( [ 'rev-parse', 'HEAD' ], self.REPO_DIR )
        self.MASTER_HEAD = output.strip()

        run_git_cmd( [ 'checkout', 'branch' ], self.REPO_DIR )
        run_git_cmd( [ 'merge', '--no-ff', 'master'], self.REPO_DIR )

    def test_with_merges( self ):
        (returncode, output) = run_git_cmd( [ 'fork-point', '-p', 'master' ], self.REPO_DIR )
        commit_hash = self.get_hash( output )
        self.assertEqual( self.MERGE_HASH, commit_hash )

    def test_after_rebase( self ):
        run_git_cmd( [ 'rebase', 'master' ], self.REPO_DIR )
        (returncode, output) = run_git_cmd( [ 'fork-point', '-p', 'master' ], self.REPO_DIR )
        commit_hash = self.get_hash( output )
        self.assertEqual( self.MASTER_HEAD, commit_hash )

    def test_from_master( self ):
        run_git_cmd( [ 'checkout', 'master' ], self.REPO_DIR )
        (returncode, output) = run_git_cmd( [ 'fork-point', '-p', 'master' ], self.REPO_DIR )
        commit_hash = self.get_hash( output )
        self.assertEqual( self.MASTER_HEAD, commit_hash )

    def get_hash( self, output ):
        lines = output.splitlines()
        return self.ANSI_REGEX.sub( '', lines[ 0 ].split()[ 1 ] )

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase( TestGitForkPoint )
    unittest.TextTestRunner( verbosity=2 ).run( suite )
