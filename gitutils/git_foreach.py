import argparse
import os
from gitutils.ansi import Ansi
from gitutils.common import find_workspace_dir, find_repo_dirs, run_cmd

def git_foreach( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git foreach',
            description='Execute an abitrary command in each Git repo'
    )
    parser.add_argument( 'cmd', nargs='*' )
    opts = parser.parse_args( args )

    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        print( '{}{}{}'.format( Ansi.GREEN, os.path.basename( repo_dir ), Ansi.RESET ) )
        (returncode, output) = run_cmd( args, repo_dir )
        for line in output.splitlines():
            print( '  ' + line )

    return 0
