import os
import sys
import argparse
from gitutils.ansi import Ansi
from gitutils.common import run_git_cmd, current_branch, is_valid_branch, is_valid_remote_branch, find_repo_dirs, find_workspace_dir

def git_cleanup_branch( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git cleanup-branch',
            description='Deletes local, remote-tracking, and remote instances of given branch'
    )
    parser.add_argument( '-a', '--all', action='store_true', default=False, help="perform cleanup on all repos in the workspace" )
    parser.add_argument( 'branch', help='name of branch to delete' )
    opts = parser.parse_args( args )

    if opts.all:
        for repo_dir in find_repo_dirs( find_workspace_dir() ):
            if is_valid_branch( repo_dir, opts.branch ):
                cleanup_branch( repo_dir, opts.branch )
    else:
        cleanup_branch( os.getcwd(), opts.branch )


def cleanup_branch( repo_dir, branch ):
    repo_name = os.path.basename( repo_dir )
    print( '{}{}:{}'.format( Ansi.UNDERLINE, repo_name, Ansi.RESET ) )

    # ensure that we're not on the branch that is being deleted
    if current_branch( repo_dir )[ 1 ] == branch:
        print( '    {}Cannot remove a branch that is currently checked out.  Switch to another branch first{}'.format( Ansi.RED, Ansi.RESET ) )
        return

    # fetch latest remotes which will cleanup remote-tracking branch if someone else has already deleted remote branch
    (returncode, output) = run_git_cmd( [ 'fetch', '--all', '--prune' ], repo_dir )
    if returncode != 0:
        for line in output.splitlines():
            print( '{}{}{}'.format( Ansi.RED, line, Ansi.RESET ) )
        return

    if is_valid_remote_branch( repo_dir, branch ):
        # remote still exists so we'll delete it, along with the remote-tracking branch
        (returncode, output) = run_git_cmd( [ 'push', 'origin', '--delete', branch ], repo_dir )
        if returncode != 0:
            for line in output.splitlines():
                print( "    {}{}{}".format( Ansi.RED, line, Ansi.RESET ) )
            return

    print( '    removed remote branch' )

    if is_valid_branch( repo_dir, branch ):
        # local branch exists so try to delete it
        (returncode, output) = run_git_cmd( [ 'branch', '-d', branch ], repo_dir )
        if returncode != 0:
            print( "    {}branch is not fully merged, are you sure you'd like to remove it?{} [y/n]".format( Ansi.YELLOW, Ansi.RESET ) )
            response = sys.stdin.readline().rstrip().lower()
            while response != 'y' and response != 'n':
                print( "You must enter either 'y' or 'n'" )
                response = sys.stdin.readline().rstrip().lower()

            if response.lower() == 'y':
                (returncode, output) = run_git_cmd( [ 'branch', '-D', branch ], repo_dir )
                if returncode != 0:
                    for line in output.splitlines():
                        print( "    {}{}{}".format( Ansi.RED, line, Ansi.RESET ) )
                else:
                    print( '    removed local branch' )
            elif response.lower() == 'n':
                print( "    Skipping local branch '{}'".format( branch ) )
        else:
            print( '    removed local branch' )

    return 0
