import argparse
import os
import re
from datetime import date, timedelta
from gitutils.ansi import Ansi
from gitutils.common import find_workspace_dir, find_repo_dirs, git_fetch, git_tag, is_valid_branch, git_checkout, current_branch, DATE_PATTERN

def git_latest_tags( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git latest-tags',
            description='''For all repos in the workspace, checks out the latest tag

A regex pattern can optionally be provided to restrict allowed tags to those that match a particular regex pattern.
If the -m option is used but no pattern is specified, then a default date pattern is assumed (YYYY.MM.DD).
In all cases, if a tag contains a date pattern that is greater than or equal to today's date, then it will be ignored.
''',
            formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument( '-m', '--match', metavar='PATTERN', default='.*', help="only checks out a tag if it matches supplied pattern" )
    parser.add_argument( '-w', '--wednesday', action='store_true', default=False, help="only matches tags with last wednesday's date pattern" )
    parser.add_argument( '-f', '--fetch', action='store_true', default=False, help="fetch before checking for valid tag" )
    parser.add_argument( '-v', '--verbose', action='store_true', default=False, help="print verbose output" )
    opts = parser.parse_args( args )

    filter_pattern = get_filter_pattern( opts.match, opts.wednesday )
    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        if opts.fetch:
            # make sure remote-tracking branches are up-to-date
            git_fetch( repo_dir )
        switch_to_latest_tag( repo_dir, filter_pattern, opts.verbose )

    return 0

def get_filter_pattern( match_pattern, use_wednesday_tag ):
    if use_wednesday_tag:
        today = date.today()
        days_since_wednesday = (today.weekday() - 2) % 7
        # if today is wednesday, consider last wednesday to be a week ago
        days_since_wednesday = days_since_wednesday if days_since_wednesday != 0 else 7
        last_wednesday = today - timedelta( days=days_since_wednesday )
        return last_wednesday.strftime( '%Y.%m.%d' )
    else:
        return match_pattern

def switch_to_latest_tag( repo_dir, filter_pattern, verbose ):
    pattern = re.compile( filter_pattern )
    repo_name = os.path.basename( repo_dir )

    # iterate over tags in reverse order, starting with most recent
    for tag in reversed( git_tag( repo_dir, 'taggerdate' ) ):
        if pattern.match( tag ):
            # switching to a tag while in a detached HEAD state
            # doesn't always work so first switch to master branch
            if is_valid_branch( repo_dir, 'master' ):
                git_checkout( repo_dir, 'master', False )

            # now switch to the desired tag
            git_checkout( repo_dir, tag, False )
            print( "{}{}:{} {}switched to {}{}".format(
                    Ansi.UNDERLINE,
                    repo_name,
                    Ansi.RESET,
                    Ansi.GREEN,
                    current_branch( repo_dir )[ 1 ],
                    Ansi.RESET
            ) )
            return True

    print( "{}{}:{} no valid tags found, remaining in {}".format(
            Ansi.UNDERLINE,
            repo_name,
            Ansi.RESET,
            current_branch( repo_dir )[ 1 ]
    ) )
    return False
