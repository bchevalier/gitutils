#!/usr/bin/env python

import argparse
import os
import shutil
import sys
import tempfile
import tarfile
import subprocess
import pkg_resources
import platform
import contextlib
import json
import codecs
from urllib2 import urlopen
from urlparse import urljoin
from distutils.version import StrictVersion

"""
GitUtils installer

Maintained at https://gitlab.com/bchevalier/gitutils

Run this script to install or upgrade GitUtils
"""

BASE_URL = 'https://bchevalier.gitlab.io/gitutils/'
VERSION_URL = urljoin( BASE_URL, 'version.json' )
RELEASE_URL = urljoin( BASE_URL, 'release/' )

def update( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='update',
            description='''Install or update GitUtils'''
    )
    parser.add_argument( '-i', '--installed_version', metavar='VERSION', default='', help='version currently installed' )
    opts = parser.parse_args( args )

    available = get_latest_available_version()
    if ( opts.installed_version
            and StrictVersion( opts.installed_version ) >= StrictVersion( available ) ):
        print( 'Latest GitUtils version is already installed: {}'.format( available ) )
        return 0
    else:
        archive = download_source( available )
        return install( archive )

def download_source( version ):
    download_dir = os.path.abspath( os.curdir )
    tarfile = "gitutils-{}.tar.gz".format( version )
    url = urljoin( RELEASE_URL, tarfile )
    saveto = os.path.join( download_dir, tarfile )

    if not os.path.exists( saveto ):
        print( 'Downloading {}'.format( url ) )
        downloader = get_best_downloader()
        downloader( url, saveto )
    else:
        print( '{} already exists, skipping download'.format( saveto ) )
    return os.path.realpath( saveto )

def install( archive_filename ):
    """Unpack and install GitUtils using setup.py python script"""
    with archive_context( archive_filename ):
        print( 'Installing GitUtils' )
        cmd = [ sys.executable, 'setup.py', 'install' ]
        if not subprocess.call( cmd ) == 0:
            print( 'Something went wrong during the installation' )
            return 2

    # ensure that tab completion file is sourced
    filename = '.bash_profile' if platform.system() == 'Darwin' else '.bashrc'
    path = os.path.join( os.path.expanduser( '~' ), filename )
    with open( path, 'r+' ) as file:
        for line in file:
            if 'gitutils-completion.bash' in line:
                break
        else:
            resource = pkg_resources.resource_filename( 'gitutils', 'gitutils-completion.bash' )
            file.write( '\n' )
            file.write( '# allow tab-completion for GitUtilities\n' )
            file.write( 'if [ -f {} ]; then\n'.format( resource ) )
            file.write( '    . {}\n'.format( resource ) )
            file.write( 'fi' )
    with open( path, 'r+' ) as file:
        for line in file:
            if 'GITUTILS_REPO_ROOT' in line:
                break
        else:
            resource = pkg_resources.resource_filename( 'gitutils', 'gitutils-completion.bash' )
            file.write( '\n' )
            file.write( '# Uncomment next line to hard-code workspace dir for GitUtils\n' )
            file.write( '#export GITUTILS_REPO_ROOT=\n' )

    return 0

def get_latest_available_version():
    print( 'Retrieving latest version from {}'.format( VERSION_URL ) )
    with contextlib.closing( urlopen( VERSION_URL ) ) as resp:
        try:
            charset = resp.info().get_content_charset()
        except Exception:
            # assume UTF-8 for Python 2 compatability
            charset = 'UTF-8'
        reader = codecs.getreader( charset )
        doc = json.load( reader( resp ) )

    return str( doc[ 'version' ] )

@contextlib.contextmanager
def archive_context( filename ):
    """Untar filename to a temp directory, and clean it up afterwards"""
    old_wd = os.getcwd()
    tmpdir = tempfile.mkdtemp()
    print( 'Extracting to {}'.format( tmpdir ) )

    try:
        os.chdir( tmpdir )
        with tarfile.open( filename, 'r:*' ) as archive:
            archive.extractall()

        subdir = os.path.join( tmpdir, os.listdir( tmpdir )[ 0 ] )
        os.chdir( subdir )
        yield

    finally:
        os.chdir( old_wd )
        shutil.rmtree( tmpdir )

def quiet_check_call( cmd ):
    """Execute command, sending all output to /dev/null and return True iff cmd succeeds"""
    with open( os.path.devnull, 'wb' ) as devnull:
        try:
            subprocess.check_call( cmd, stdout=devnull, stderr=devnull )
        except subprocess.CalledProcessError:
            return False
    return True

def clean_check_call(cmd, target):
    """Attempt to download target and if cmd fails, cleanup before re-raising exception"""
    try:
        subprocess.check_call( cmd )
    except subprocess.CalledProcessError:
        if os.access( target, os.F_OK ):
            os.unlink( target )
        raise

def get_best_downloader():
    downloaders = (
        download_file_powershell,
        download_file_curl,
        download_file_wget,
        download_file_insecure,
    )
    viable_downloaders = ( dl for dl in downloaders if dl.viable() )
    return next( viable_downloaders, None )

def download_file_powershell( url, target ):
    target = os.path.abspath( target )
    ps_cmd = (
        "[System.Net.WebRequest]::DefaultWebProxy.Credentials = "
        "[System.Net.CredentialCache]::DefaultCredentials; "
        '(new-object System.Net.WebClient).DownloadFile("%(url)s", "%(target)s")'
        % locals()
    )
    cmd = [ 'powershell', '-Command', ps_cmd ]
    clean_check_call( cmd, target )

def has_powershell():
    if platform.system() != 'Windows':
        return False
    return quiet_check_call( [ 'powershell', '-Command', 'echo test' ] )
download_file_powershell.viable = has_powershell

def download_file_curl( url, target ):
    cmd = [ 'curl', url, '--location', '--silent', '--output', target ]
    clean_check_call( cmd, target )

def has_curl():
    return quiet_check_call( [ 'curl', '--version' ] )
download_file_curl.viable = has_curl

def download_file_wget( url, target ):
    cmd = [ 'wget', url, '--quiet', '--output-document', target ]
    clean_check_call( cmd, target )

def has_wget():
    return quiet_check_call( [ 'wget', '--version' ] )
download_file_wget.viable = has_wget

def download_file_insecure( url, target ):
    """Use Python to download the file, without connection authentication"""
    with contextlib.closing( urlopen( url ) ) as src:
        # read all the data in one block
        data = src.read()
    # write all data in one block to avoid creating a partial file
    with open( target, "wb" ) as dst:
        dst.write( data )
download_file_insecure.viable = lambda: True

if __name__ == '__main__':
    sys.exit( update( sys.argv[ 1: ] ) )
