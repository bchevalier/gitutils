import argparse
import os
from gitutils.ansi import Ansi
from gitutils.common import find_workspace_dir, find_repo_dirs, current_branch, is_valid_remote_branch, run_git_cmd

def git_pull_all( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git pull-all',
            description='''Updates all repos in the workspace from their remotes

If a repo's current branch is not in detached HEAD state and has an upstream
branch, a pull --rebase is executed, otherwise nothing will be done.''',
            formatter_class=argparse.RawTextHelpFormatter
    )
    opts = parser.parse_args( args )

    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        repo_name = os.path.basename( repo_dir )
        print( '{}{}{}'.format( Ansi.UNDERLINE, repo_name, Ansi.RESET ) )

        # verify that repo's current branch has a remote to pull from
        (is_detached, branch) = current_branch( repo_dir ) 
        if not is_detached and is_valid_remote_branch( repo_dir, branch ):
            (returncode, output) = run_git_cmd( [ 'pull', '--rebase' ], repo_dir )

            color = Ansi.RESET if returncode == 0 else Ansi.RED
            for line in output.splitlines():
                print( '  {}{}{}'.format( color, line, Ansi.RESET ) )
        else:
            print( "{}  Skipping because no remote exists for branch '{}'{}".format( Ansi.YELLOW, branch, Ansi.RESET ) )

    return 0
