import os
import sys
import argparse
from gitutils.common import find_workspace_dir, is_in_repo

def git_ignore( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git ignore',
            description='''Adds given patterns to .gitignore of current repo'''
    )
    parser.add_argument( 'patterns', nargs='+', help='one or more patterns to be added to .gitignore' )
    parser.add_argument( '-r', '--repo', help="repo to be updated" )
    opts = parser.parse_args( args )

    filename = ''
    if opts.repo:
        # repo name was given, try to find absolute path
        workspace = find_workspace_dir()
        repo_base_dir = os.path.join( workspace, opts.repo )
        if os.path.isdir( repo_base_dir ):
            filename = os.path.join( repo_base_dir, '.gitignore' )
        else:
            print( 'Invalid directory: {}'.format( repo_base_dir ) )
            sys.exit( 2 )
    else:
        # assume that we are inside the relevant git repo
        (in_repo, repo_base_dir) = is_in_repo( os.getcwd() )
        if in_repo:
            filename = os.path.join( repo_base_dir, '.gitignore' )
        else:
            print( 'Unable to find git repo from: {}'.format( os.getcwd() ) )
            sys.exit( 2 )
    
    with open( filename, 'ab+' ) as file:
        # ensure that the file is either empty or ends with a newline
        if file.tell() != 0:
            file.seek( -1, 2 )
            last_byte = file.read( 1 )
            if last_byte != '\n':
                file.write( '\n' )

         # append each new pattern
        for pattern in opts.patterns:
            file.write( pattern + '\n' )

    return 0
