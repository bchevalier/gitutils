import argparse
import os
from gitutils.ansi import Ansi
from gitutils.common import find_workspace_dir, find_repo_dirs, current_branch, is_valid_remote_branch, run_git_cmd, git_fetch

def git_rebase_all( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git rebase-all',
            description='''Rebases all repos in the workspace onto the given branch

If a repo's current branch is not in detached HEAD state and is different than target
branch, a git rebase <branch> is executed, otherwise nothing will be done.''',
            formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument( 'branch', help='name of branch to rebase on' )
    parser.add_argument( '-f', '--fetch', action='store_true', default=False, help="fetch before rebasing each repo" )
    opts = parser.parse_args( args )

    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        if opts.fetch:
            # make sure remote-tracking branches are up-to-date
            git_fetch( repo_dir )

        repo_name = os.path.basename( repo_dir )
        print( '{}{}{}'.format( Ansi.UNDERLINE, repo_name, Ansi.RESET ) )

        # verify that repo's current branch has a remote to pull from
        (is_detached, branch) = current_branch( repo_dir ) 
        if is_detached:
            print( '{}  Skipping because HEAD is detached{}'.format( Ansi.YELLOW, Ansi.RESET ) )
        elif branch == opts.branch:
            print( '{}  Skipping because current branch matches target branch{}'.format( Ansi.YELLOW, Ansi.RESET ) )
        else:
            (returncode, output) = run_git_cmd( [ 'rebase', opts.branch ], repo_dir )
            format_output( returncode, output )

def format_output( returncode, output ):
    lines = output.splitlines()

    color = Ansi.RESET
    for line in lines:
        if returncode != 0 and ( line.startswith( 'Cannot rebase' ) or line.startswith( 'CONFLICT' ) ):
            color = Ansi.RED
        print( '{}  {}{}'.format( color, line, Ansi.RESET ) )

    return 0
