import os
import argparse
from gitutils.ansi import Ansi
from gitutils.common import run_cmd, run_git_cmd, find_workspace_dir, find_repo_dirs, git_fetch, is_valid_branch, fork_point_commit_hash, git_checkout, current_branch, DATE_PATTERN
from gitutils.git_latest_tags import get_filter_pattern, switch_to_latest_tag

def git_move_to_tags( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git move-to-tags',
            description='''Cherry-picks commits from given branch onto latest tag

Takes all non-merge commits since the fork-point on the given branch, and cherry-picks
them onto the latest tag branch, if one exists.  Uses git-latest-tags to checkout the
latest tag for all branches in the workspace, and then uses git-fork-point, for each
repo that contains the given branch, to determine the first commit to be considered
for cherry-picking.  Any merge commits between the fork point and HEAD of the given
branch will be ignored.''',
            formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument( '-p', '--parent', metavar='BRANCH', default='origin/master', help='parent branch of current branch' )
    parser.add_argument( '-m', '--match', metavar='PATTERN', default='.*', help="only checks out a tag if it matches supplied pattern" )
    parser.add_argument( '-w', '--wednesday', action='store_true', default=False, help="only matches tags with last wednesday's date pattern" )
    parser.add_argument( '-f', '--fetch', action='store_true', default=False, help="fetch before checking for valid tag" )
    parser.add_argument( '-c', '--create', action='store_true', default=False, help="create new branch based off tag before applying commits" )
    parser.add_argument( 'branch', help='name of branch to be cherry-picked onto tags' )
    opts = parser.parse_args( args )

    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        if opts.fetch:
            # make sure remote-tracking branches are up-to-date
            git_fetch( repo_dir )

        if is_valid_branch( repo_dir, opts.branch ):
            # repo contains given branch so find fork commit
            (returncode, fork_commit) = fork_point_commit_hash( repo_dir, opts.parent, opts.branch )
            if returncode != 0:
                print( '{}Unable to get commit hash of fork point for repo: {}{}'.format(
                        Ansi.RED,
                        os.path.basename( repo_dir ),
                        Ansi.RESET
                ) )
                continue

            # attempt to checkout latest tag that matches given pattern
            filter_pattern = get_filter_pattern( opts.match, opts.wednesday )
            if switch_to_latest_tag( repo_dir, filter_pattern, False ):
                tag_name = current_branch( repo_dir )[ 1 ]
                target_branch = '{}_{}'.format( opts.branch, tag_name ) if opts.create else tag_name
                if opts.create:
                    git_checkout( repo_dir, target_branch, True )

                # git all commits to be cherry-picked onto tag
                (returncode, output) = run_git_cmd( [ 'rev-list', '--first-parent', '--no-merges', '--reverse', '{}..{}'.format( fork_commit, opts.branch ) ], repo_dir )
                if returncode != 0:
                    print( '{}Unable to find any cherry-pick commits after {} for repo: {}{}'.format(
                            Ansi.YELLOW,
                            fork_commit,
                            os.path.basename( repo_dir ),
                            Ansi.RESET
                    ) )
                    print( output )
                    continue

                # cherry-pick each commit
                conflict = False
                for commit in output.splitlines():
                    if not conflict:
                        print( '    cherry-picking {} onto {}'.format( commit, target_branch ) )
                        (returncode, output) = run_git_cmd( [ 'cherry-pick', commit ], repo_dir )
                        if returncode != 0:
                            conflict = True
                            for line in output.splitlines():
                                print( '    {}{}{}'.format( Ansi.RED, line, Ansi.RESET ) )
                            print()
                    else:
                        print( '    {}afer resolving conflicts, manually cherry-pick {}{}'.format( Ansi.RED, commit, Ansi.RESET ) )
            else:
                # repo contains given branch but no tag exists to cherry-pick onto 
                git_checkout( repo_dir, opts.branch, False )
                print( '    {}branch {} exists but no valid tag found{}'.format( Ansi.YELLOW, opts.branch, Ansi.RESET ) )
        else:
            # repo doesn't contain given branch so just try to switch to latest tag
            filter_pattern = get_filter_pattern( opts.match, opts.wednesday )
            switch_to_latest_tag( repo_dir, filter_pattern, False )

    return 0
