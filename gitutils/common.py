import os
import sys
from subprocess import Popen, STDOUT, PIPE
from os.path import isdir, join

DATE_PATTERN = '^[0-9]{4}.[0-1][0-9].[0-3][0-9]'

def run_cmd( args, working_dir, use_shell=False ):
    if use_shell:
        proc = Popen( args, stdout=PIPE, stderr=STDOUT, cwd=working_dir, shell=use_shell, universal_newlines=True, executable='/bin/bash' )
    else:
        proc = Popen( args, stdout=PIPE, stderr=STDOUT, cwd=working_dir, shell=use_shell, universal_newlines=True )

    (stdout, stderr) = proc.communicate()
    return (proc.returncode, stdout)

def run_cmd_ignore_err( args, working_dir, use_shell=False ):
    FNULL = open( os.devnull, 'w' )
    if use_shell:
        proc = Popen( args, stdout=PIPE, stderr=FNULL, cwd=working_dir, shell=use_shell, universal_newlines=True, executable='/bin/bash' )
    else:
        proc = Popen( args, stdout=PIPE, stderr=FNULL, cwd=working_dir, shell=use_shell, universal_newlines=True )

    (stdout, stderr) = proc.communicate()
    return (proc.returncode, stdout)

def run_git_cmd( args, working_dir ):
    args.insert( 0, 'git' )
    return run_cmd( args, working_dir )

def is_in_repo( current_dir ):
    (returncode, output) = run_git_cmd( [ 'rev-parse', '--show-toplevel' ], current_dir )

    # if this is a git repo or a sub-directory of a git repo, output will contain repo's base dir
    return (True, output.rstrip()) if returncode == 0 else (False, None)

def find_workspace_dir():

    if os.environ.get('GITUTILS_REPO_ROOT'):
        return os.environ['GITUTILS_REPO_ROOT']

    # keep moving up dir levels until we find parent dir of a git repo, assume this is workspace 
    current_dir = os.path.abspath( os.getcwd() )
    while is_in_repo( current_dir )[ 0 ]:
        parent_dir = os.path.abspath( os.path.join( current_dir, '..' ) )
        if parent_dir == current_dir:
            print( 'Reached root of filesystem without finding git workspace dir from [{}]'.format( os.getcwd() ) )
            sys.exit( 2 )
        else:
            current_dir = parent_dir
    return current_dir

def find_repo_dirs( current_dir ):
    ( in_repo, repo_base_dir ) = is_in_repo( current_dir )
    if in_repo:
        # this is either a git repo or a sub-directory of a git repo, stdout contains base dir of this git repo
        return [ repo_base_dir ]
    else:
        # this is not in a git repo, check sub-directories
        child_repos = list()

        # exclude hidden directories
        for subdir in [ name for name in os.listdir( current_dir )
                        if isdir( join( current_dir, name ) ) and not name.startswith( '.' ) ]:
            ( in_repo, repo_base_dir ) = is_in_repo( join( current_dir, subdir ) )
            if in_repo:
                child_repos.append( repo_base_dir )

        return child_repos

def git_fetch( repo_dir ):
    (returncode, output) = run_git_cmd( [ 'fetch' ], repo_dir )
    if returncode != 0:
        print( 'Error while executing git_fetch( {} )'.format( repo_dir ) )
        print( output )
        sys.exit( 2 )
    return output

def is_valid_branch( repo_dir, branch_name ):
    proc = Popen( [ 'git', 'show-ref', branch_name ], stdout=PIPE, stderr=PIPE, cwd=repo_dir, universal_newlines=True )
    (stdout, stderr) = proc.communicate()
    if not stderr:
        return stdout 
    else:
        print( 'Error while executing is_valid_branch( {}, {} )'.format( repo_dir, branch_name ) )
        print( stdout )
        sys.exit( 2 )

def is_valid_remote_branch( repo_dir, branch_name ):
    proc = Popen( [ 'git', 'ls-remote', '--heads', '--exit-code', 'origin', branch_name ], stdout=PIPE, stderr=PIPE, cwd=repo_dir, universal_newlines=True )
    (stdout, stderr) = proc.communicate()
    if not stderr:
        return stdout 
    else:
        print( 'Error while executing is_valid_remote_branch( {}, {} )'.format( repo_dir, branch_name ) )
        print( stdout )
        sys.exit( 2 )

def git_checkout( repo_dir, branch_name, new_branch ):
    if new_branch:
        return run_git_cmd( [ 'checkout', '-b', branch_name ], repo_dir )
    else:
        return run_git_cmd( [ 'checkout', branch_name ], repo_dir )

def git_tag( repo_dir, sort_key='' ):
    args = [ 'tag' ]
    if sort_key:
        args.append( '--sort=' + sort_key )
    (returncode, output) = run_git_cmd( args, repo_dir )
    if returncode == 0:
        return output.rstrip().splitlines()
    else:
        print( 'Error while executing git_tag( {} )'.format( repo_dir ) )
        print( output )
        sys.exit( 2 )

def current_branch( repo_dir ):
    (returncode, output) = run_git_cmd( [ 'symbolic-ref', '--short', '-q', 'HEAD' ], repo_dir )
    if returncode == 0:
        # we're not in detached-head mode so the output should contain the branch name
        return (False, output.rstrip())
    else:
        # we're in detached-head mode so figure out which tag/commit we're detached at
        # if two tags point at same commit, then git describe is unreliable, so try reflog first
        (returncode, output) = run_git_cmd( [ 'log', '-g', '--abbrev-commit', '--pretty=oneline', '--grep-reflog=checkout' ], repo_dir )
        if returncode == 0 and output:
            first_reflog = output.splitlines()[ 0 ]
            last_checkout_target = first_reflog.split()[ -1 ]
            return (True, last_checkout_target)
        else:
            # reflog may have been purged so resort to using git describe
            (returncode, output) = run_git_cmd( [ 'describe', '--contains' ], repo_dir )
            if returncode == 0:
                return (True, output.rstrip().rstrip( '^0' ))
            else:
                # we're in detached-head mode and have made additional commits on top of a tag
                (returncode, output) = run_git_cmd( [ 'describe' ], repo_dir )
                return (True, output.rstrip())

def fork_point_commit_hash( repo_dir, parent_branch, branch ):
    # find the commit hash of the fork point
    (returncode, output) = run_cmd_ignore_err( [ "head -n 1 <(diff --old-line-format='' --new-line-format='' <(git rev-list --first-parent {}) <(git rev-list --first-parent {}))".format( parent_branch, branch ) ], repo_dir, use_shell=True )
    commit_hash = output.strip() if returncode == 0 else output
    return (returncode, commit_hash)
