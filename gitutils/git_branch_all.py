import os
import argparse
from collections import defaultdict
from gitutils.ansi import Ansi
from gitutils.common import find_workspace_dir, find_repo_dirs, run_git_cmd

def git_branch_all( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git branch-all',
            description='Prints unique workspace branches and all repos that contain them'
    )
    opts = parser.parse_args( args )

    branch_dict = defaultdict( list  )
    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        repo_name = os.path.basename( repo_dir )

        (returncode, local_branches) = run_git_cmd( [ 'for-each-ref', '--format=%(refname:short)', 'refs/heads/' ], repo_dir )
        for branch in local_branches.splitlines():
            branch_dict[ branch ].append( repo_name )

    # filter out master branch
    filtered_dict = { k:v for k,v in branch_dict.items() if 'master' not in k }

    # find max number of characters in any of the branch names
    max_width = max( len( key ) for key in filtered_dict.keys() ) + 3

    for K, V in sorted( filtered_dict.iteritems() ):
        print( '{}{}{}{}{}'.format(
                (K + ' ').ljust( max_width, '.' ),
                Ansi.GREEN,
                '\n{:<{}}'.format( '', max_width ).join( V ),
                Ansi.RESET,
                '\n'
        ) )

    return 0
