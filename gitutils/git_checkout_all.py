import argparse
import os
from gitutils.ansi import Ansi
from gitutils.common import find_workspace_dir, find_repo_dirs, git_fetch, is_valid_branch, git_checkout, current_branch

def git_checkout_all( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git checkout-all',
            description='''Check out the given branch for all repos in the workspace

If branch does not exist for a repo, nothing will be done''',
            formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument( 'branch', help='name of branch to switch to' )
    parser.add_argument( '-f', '--fetch', action='store_true', default=False, help="fetch before updating each repo" )
    opts = parser.parse_args( args )

    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        if opts.fetch:
            # make sure remote-tracking branches are up-to-date
            git_fetch( repo_dir )

        # verify that desired branch exists for this repo 
        repo_name = os.path.basename( repo_dir )
        if is_valid_branch( repo_dir, opts.branch ):
            # checkout desired branch
            (returncode, output) = git_checkout( repo_dir, opts.branch, False )
            if returncode == 0:
                print( "{}{}:{}{} switched to branch {}{}".format(
                        Ansi.UNDERLINE,
                        repo_name,
                        Ansi.RESET,
                        Ansi.GREEN,
                        current_branch( repo_dir )[ 1 ],
                        Ansi.RESET
                ) )
            else:
                print( '{}{}:{}'.format( Ansi.UNDERLINE, repo_name, Ansi.RESET ) )
                print( '{}{}{}'.format( Ansi.RED, output.strip(), Ansi.RESET ) )
        else:
            print( "{}{}:{} branch not found, skipping...".format(
                    Ansi.UNDERLINE,
                    repo_name,
                    Ansi.RESET
            ) )

    return 0
