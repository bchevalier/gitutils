import argparse
import os
from gitutils.common import find_workspace_dir, find_repo_dirs, git_fetch

def git_fetch_all( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git fetch-all',
            description='Updates remote-tracking branches for all repos in workspace'
    )
    opts = parser.parse_args( args )

    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        print( os.path.basename( repo_dir ) )
        output = git_fetch( repo_dir )
        for line in output.splitlines():
            if line and not line.startswith( 'From' ):
                print( line )

    return 0
