import os
import argparse
from gitutils.ansi import Ansi
from gitutils.common import run_cmd, run_git_cmd, fork_point_commit_hash

def git_fork_point( args, repo_dir=os.getcwd() ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git fork-point',
            description='''Finds where current branch was forked from parent branch

The fork point is the commit hash of the commit prior to this branch's earliest
commit. If current branch was not based off of master and that branch has not yet
been merged into master, then it is advisable to supply the --parent argument.''',
            formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument( '-v', '--verbose', action='store_true', default=False, help="execute 'git show' with fork point hash" )
    parser.add_argument( '-p', '--parent', metavar='BRANCH', default='origin/master', help='parent branch of current branch' )
    opts = parser.parse_args( args )

    # find the commit hash of the fork point
    (returncode, commit_hash) = fork_point_commit_hash( repo_dir, opts.parent, 'HEAD' )
    if returncode != 0:
        print( 'Error while executing git-fork-point' )
        print( commit_hash )
        return returncode

    # use 'git show' to retrieve the details of this commit
    (returncode, details) = run_git_cmd( [ 'show', commit_hash.rstrip() ], repo_dir )
    if returncode != 0:
        print( "Error while executing 'git show {}'".format( commit_hash.rstrip() ) )
        print( details )
        return returncode

    if opts.verbose:
        # split detail into lines and then apply coloring
        for line in [ x for x in details.splitlines() if x ]:
            format_line( line ) 
    else:
        # split detail into lines but just print the commit hash and summary
        for line in [ x for x in details.splitlines() if x ]:
            if line.startswith( 'diff'):
                break;
            format_line( line ) 

    return 0

def format_line( line ):
    if line.startswith( 'commit ' ):
        print( '{}{}{}'.format( Ansi.YELLOW, line, Ansi.RESET ) )
    elif line.startswith( '@@' ):
        print( '{}{}{}'.format( Ansi.BLUE, line, Ansi.RESET ) )
    elif line.startswith( '-' ):
        print( '{}{}{}'.format( Ansi.RED, line, Ansi.RESET ) )
    elif line.startswith( '+' ):
        print( '{}{}{}'.format( Ansi.GREEN, line, Ansi.RESET ) )
    else:
        print( '{}'.format( line ) )
