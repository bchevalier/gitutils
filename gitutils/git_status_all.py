import argparse
import os
import sys
from gitutils.ansi import Ansi
from gitutils.common import find_workspace_dir, find_repo_dirs, current_branch, run_git_cmd, git_fetch

def git_status_all( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git status-all',
            description='Prints the status of all repos in the workspace'
    )
    parser.add_argument( '-v', '--verbose', action='store_true', default=False, help='print verbose status info' )
    parser.add_argument( '-f', '--fetch', action='store_true', default=False, help="fetch before displaying status of each repo" )
    opts = parser.parse_args( args )

    for repo_dir in find_repo_dirs( find_workspace_dir() ):
        if opts.fetch:
            # make sure remote-tracking branches are up-to-date
            git_fetch( repo_dir )

        (returncode, output) = run_git_cmd( [ 'status' ], repo_dir )
        if returncode == 0:
            # split stdout into lines
            lines = output.splitlines()

            # print repo and and current branch
            repo_name = os.path.basename( repo_dir )
            print( '{}{}{}{}{}'.format(
                    get_highest_alert_color( lines ),
                    Ansi.UNDERLINE,
                    repo_name.ljust( 34 ),
                    current_branch( repo_dir )[ 1 ].rjust( 66 ),
                    Ansi.RESET
            ))

            # print formatted status
            format_status( lines, opts.verbose )
        else:
            print( 'Error while executing git-status-all( {} )'.format( repo_dir ) )
            print( stdout )
            return returncode

    return 0

def get_highest_alert_color( status_lines ):
    # loop through lines to determine color associated with highest alert level
    alert_color = Ansi.RESET
    for line in status_lines:
        if (line.startswith( 'Changes not staged for commit' ) or line.startswith( 'rebase in progress' ) ):
            alert_color = Ansi.RED
        elif ( line.startswith( 'Your branch is behind' ) and alert_color != Ansi.RED ):
            alert_color = Ansi.YELLOW
        elif ( line.startswith( 'Untracked files' ) and
               alert_color != Ansi.RED and
               alert_color != Ansi.YELLOW ):
            alert_color = Ansi.BLUE
        elif ( line.startswith( 'Changes to be committed' ) and
               alert_color != Ansi.RED and
               alert_color != Ansi.YELLOW and
               alert_color != Ansi.BLUE ):
            alert_color = Ansi.GREEN
    return alert_color

def format_status( status_lines, verbose ):
    color = Ansi.RESET

    for line in status_lines:
        if ( line and
             not line.startswith( 'On branch' ) and
             not line.startswith( 'HEAD detached at' ) and
             not line.startswith( 'Your branch is up-to-date' ) and
             not line.startswith( 'Your branch is up to date' ) and
             not line.startswith( 'nothing to commit' ) and
             not line.startswith( 'nothing added' ) and
             not line.startswith( 'no changes' ) ):

            if line.startswith( 'Your branch is behind' ):
                print( '  {}{}{}'.format( Ansi.YELLOW, line, Ansi.RESET ) )
            elif line.startswith( 'Changes to be committed' ):
                print( '  {}'.format( line ) )
                color = Ansi.GREEN
            elif ( line.startswith( 'Changes not staged for commit' ) or
                   line.startswith( 'Unmerged paths' ) ):
                print( '  {}'.format( line ) )
                color = Ansi.RED
            elif line.startswith( 'Untracked files' ):
                print( '  {}'.format( line ) )
                color = Ansi.BLUE
            elif ( line.startswith( '  (use "git reset' ) or
                   line.startswith( '  (use "git add' ) or
                   line.startswith( '  (fix conflicts and then run' ) or
                   line.startswith( '  (use "git rebase' ) or
                   line.startswith( '  (use "git checkout' ) ):
                if verbose:
                    print( '  {}'.format( line ) )
            else:
                print( '  {}{}{}'.format( color, line, Ansi.RESET ) )
