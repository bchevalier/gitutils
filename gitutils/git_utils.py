import argparse
import contextlib
import os
import sys
import StringIO
from os.path import isfile, join
from gitutils.ansi import Ansi
from gitutils.update import update

def git_utils( args ):
    # create argument parser and retrieve options
    parser = argparse.ArgumentParser(
            prog='git utils',
            description='Prints info about the available GitUtils commands'
    )
    parser.add_argument( '-d', '--details', action='store_true', default=False, help='print detailed command info' )
    parser.add_argument( '-m', '--markdown', action='store_true', default=False, help='print command info in markdown format' )
    parser.add_argument( '-v', '--version', action='store_true', default=False, help='print installed version of GitUtils' )
    parser.add_argument( '-u', '--update', action='store_true', default=False, help='install the latest version of GitUtils' )
    opts = parser.parse_args( args )

    if opts.version:
        # print version option is mutually exclusive of all other options
        import gitutils
        print( gitutils.__version__ )
        return 0
    elif opts.update:
        # update version option is mutually exclusive of all other options
        import gitutils
        update( [ '-i', gitutils.__version__ ] )
        return 0
    elif opts.markdown:
        # if outputting in markdown format, print a header
        print( '## Commands' )

    # process all modules in same dir as this file that begin with the prefix 'git_'
    lib_dir = os.path.dirname( os.path.realpath( __file__ ) )
    for filename in [
            name for name in os.listdir( lib_dir )
            if isfile( join( lib_dir, name ) )
            and name.startswith( 'git_' )
            and not name.endswith( '.pyc' )
    ]:
        # use reflection to find the method that matches the module name
        module_name = os.path.splitext( filename )[ 0 ]
        module = __import__( 'gitutils.' + module_name, fromlist=[ module_name ] )
        method = getattr( module, module_name )

        try:
            # need to redirect the ouput of the method call from stdout
            output = StringIO.StringIO()
            with stdout_redirect( output ):
                method( [ '-h' ] )
        except SystemExit:
            # exit exception will be thrown when printing usage info
            pretty_print( output.getvalue(), opts.details, opts.markdown )
            output.close()
    return 0

def pretty_print( output, details, markdown ):
    for line in output.splitlines():
        if line.startswith( 'usage' ):
            if details or markdown:
                print( '' )
            if markdown:
                print( '###{}'.format( line[ 7: ] ) )
            else:
                print( '{}{}{}'.format( Ansi.GREEN, line[ 7: ], Ansi.RESET ) )
        elif details or markdown:
            print( '    {}'.format( line ) )

@contextlib.contextmanager
def stdout_redirect( stream ):
    old_stdout = sys.stdout
    sys.stdout = stream
    try:
        yield
    finally:
        sys.stdout = old_stdout

if( __name__ == "__main__" ):
    git_utils( sys.argv[ 1: ] )
